import React from "react";
import { UserList } from "./UserList";

export const Home = () => {
  return (
    <React.Fragment>
      <div>
        <header className="App-header">
          <h1 className="App-title">User Management</h1>
        </header>        
        <UserList />
      </div>
    </React.Fragment>
  );
};