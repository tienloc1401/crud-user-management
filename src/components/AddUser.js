import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { nanoid } from 'nanoid';
import { GlobalContext } from '../context/GlobalState';
import {
  Button,
  Modal,
  Form,
  InputGroup,
} from "react-bootstrap";

export const AddUser = () => {
  let history = useHistory();

  const { addUser, users } = useContext(GlobalContext);

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [birth_day, setBirthday] = useState("");
  const [validated, setValidated] = useState(false);

  const onSubmit = (e) => {
    const form = e.currentTarget
    if (form.checkValidity() === false) {
      e.preventDefault()
      e.stopPropagation()
    }

    else{
      setValidated(true);      
      const newUser = {        
        name,
        phone,
        birth_day,
      };
      addUser(newUser);
      history.push("/");
      e.preventDefault();
    }
  };

  return (
    <React.Fragment>   
      <Form noValidate validated={validated} onSubmit={onSubmit}>   
        <Modal.Dialog >
          <Modal.Header>
            <Modal.Title>User Details</Modal.Title>
          </Modal.Header>                  
          <Modal.Body>                    
            <Form.Group className="mb-3" controlId="formControlName">
              <Form.Label>Name</Form.Label>
              {/* <InputGroup hasValidation> */}                
                <Form.Control
                  type='name'
                  value={name}
                  required
                  onChange={(e) => setName(e.target.value)}
                  placeholder="User name"
                />
                {/* <Form.Control.Feedback type="invalid">
                  Please enter your name.
                </Form.Control.Feedback> */}
              {/* </InputGroup> */}
            </Form.Group>

            <Form.Group className="mb-3" controlId="formControlPhone">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type='string'
                value={phone}
                required            
                onChange={(e) => setPhone(e.target.value)}    
                placeholder="Enter phone number"          
              />
            </Form.Group>
        
            <Form.Group className="mb-3" controlId="formControlBD">
              <Form.Label>Birthday</Form.Label>
              <Form.Control
                type='date'
                value={birth_day} 
                required           
                onChange={(e) => setBirthday(e.target.value)}              
              />
            </Form.Group>         
          </Modal.Body>
          
          <Modal.Footer>
            <Button variant="outline-secondary" href='/'>Back</Button>            
            <Button variant="outline-primary" type='submit'>Save</Button>
          </Modal.Footer> 
        </Modal.Dialog>
      </Form>
    </React.Fragment>
  );
};