import React, { useState, useContext, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';

import { GlobalContext } from '../context/GlobalState';
import {
  Button,
  Modal,
  Form,

} from "react-bootstrap";

export const EditUser = (route) => {
  let history = useHistory();

  const { data, editUser } = useContext(GlobalContext);
  
  const [selectedUser, setSelectedUser] = useState({
    id: null,
    name: "",
    phone:"",
    birth_day: "",
  });
  
  const currentUserId = route.match.params.id;
  
  useEffect(() => {
    const userId = currentUserId;
    const selectedUser = data.find(
      (currentUserTraversal) => currentUserTraversal.id === parseInt(userId)
    );
    setSelectedUser(selectedUser);
    
  }, [currentUserId, data]);
  
  console.log(currentUserId)
  console.log(selectedUser.id)
  
  if (!currentUserId) {
    return <div>Invalid User ID.</div>;
  }

  const handleOnChange = (userKey, newValue) =>
    setSelectedUser({ ...selectedUser, [userKey]: newValue });
  
  const onSubmit = (e) => {
    e.preventDefault();
    editUser(selectedUser);
    history.push("/");
  };

  return (
    <React.Fragment>
      <Form onClick={onSubmit}>       
      <Modal.Dialog >
        <Modal.Header>
          <Modal.Title>Edit User Details</Modal.Title>
        </Modal.Header>        
        <Modal.Body>         
          <Form.Group className="mb-3" controlId="formControlName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type='name'
              value={selectedUser.name}
              required
              onChange={(e) => handleOnChange("name", e.target.value)}              
            />
          </Form.Group>
          
          <Form.Group className="mb-3" controlId="formControlBD">
            <Form.Label>Birthday</Form.Label>
            <Form.Control
              type='date'
              value={selectedUser.birth_day}            
              onChange={(e) => handleOnChange("birth_day", e.target.value)}     
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formControlPhone">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control
              type='string'
              value={selectedUser.phone}            
              onChange={(e) => handleOnChange("phone", e.target.value)}                          
            />
          </Form.Group>                
        </Modal.Body>        
        <Modal.Footer>
          <Button variant="outline-secondary" >
            <Link to="/">Back</Link>
          </Button>
          <Button variant="outline-primary" type='submit' >Edit</Button>
        </Modal.Footer>
      </Modal.Dialog>
      </Form>
    </React.Fragment>
  );
};