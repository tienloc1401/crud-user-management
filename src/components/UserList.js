import React, { useContext} from 'react';
import { Link } from 'react-router-dom';

import { GlobalContext } from '../context/GlobalState';
import {
  Button,
  Card,
  Table,
  
} from "react-bootstrap";
import { FaPencilAlt, FaTrashAlt } from "react-icons/fa";

export const UserList = () => {
  const { data, removeUser } = useContext(GlobalContext);
  
  const customButton = {
    color: "white",
    background: "linear-gradient(to right, rgb(238,130,238), rgb(0,0,255))",
    padding: "10px",
  }

  const customCard = {
    padding: "20px",
    marginTop: "20px",
    marginBottom: "20px"
  }

  const editUser = id => {
    window.location = '/edit/'+id
  }


  

  return (
    <React.Fragment>
      <Button 
        href='/add'
        variant="outline-secondary" 
        style={customButton}>Add User
      </Button>
      <Card>
        <Card.Body style={customCard}>
          <Table striped bordered hover variant="light">
            <thead>
              <tr>
                {/* <th>#</th> */}
                <th>Name</th>
                <th>Phone</th>
                <th>Birthday</th>
                <th>Actions</th>
              </tr>                  
            </thead>
            <tbody>
              {data.length > 0 ? (
                data.map((user) => (
                  <tr>
                      {/* <td>{user.id}</td> */}
                      <td>{user.name}</td>
                      <td>{user.phone}</td>
                      <td>{user.birth_day}</td>                  
                      <td>                      
                        <Button 
                          variant="info"
                          title="Edit User"
                          onClick={() => editUser(user._id)}>
                          <FaPencilAlt />    
                          </Button>                
                        <Button
                          variant="danger"
                          title="Delete User"
                          onClick={() => removeUser(user._id)}
                        >
                          <FaTrashAlt />
                        </Button>
                      </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan={4}className="text-center">
                    No data found
                  </td>
                </tr>  
              )}                
            </tbody>      
          </Table>
        </Card.Body>
      </Card> 
    </React.Fragment>
  );
};