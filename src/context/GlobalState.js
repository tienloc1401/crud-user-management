import React, { createContext, useReducer, useState, useEffect } from 'react';
import axios from 'axios';
import appReducer from './AppReducer';

// const initialState = {
//   users: [
//     {
//       id: 1,
//       name: 'Tô Đoàn Hiển Vinh',
//       phone: '0335177666',
//       birth_day: '1998-02-10',
//     }
//   ]
// };


export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [data, setData] = useState([])
  // const [state, dispatch] = useReducer(appReducer, data);  

  useEffect(() => {
    axios.get('http://192.168.9.20:5000/api/users').then((response) => {
      if(response.status === 200){
        setData(response.data.result);
        console.log(response.data)
      }    
    });      
  }, [])
  
  async function addUser(user) {
    const res = await axios.post(
      'http://192.168.9.20:5000/api/users',
      user,
      {
        headers: { 'Content-Type': 'application/json' },
      }
    );
    console.log(res)
    if (res.status === 200){
      setData([...data, user])
      
    }
    console.log(data)
    
  }

  async function editUser(user) {
    const res = await axios.put(
      `http://192.168.9.20:5000/api/users/${user.id}`,
      user,
      {
        headers: { 'Content-Type': 'application/json' },
      }      
    );    
    if(res.status === 200){
      const editedUser = {
        id: user.id,
        name: user.name,
        phone: user.phone,
        birth_day: user.birth_day
      }
      console.log(editedUser)
      const newUsers = [...data]
      const index = data.findIndex((user) => user.id === editUser.id)
      
      newUsers[index] = editedUser;  
      setData(newUsers)      
    }
    console.log(user.id) 
  }

  async function removeUser(id) {
    const res = await axios.delete(
      `http://192.168.9.20:5000/api/users/${id}`,
      id,
      {
        headers: { 'Content-Type': 'application/json' },
      }
    );
    
    if (res.status === 200){
      const newUsers= [...data]
      const index = data.filter((user) => user.id === id)
      newUsers.splice(index, 1)
      setData(newUsers)      
    }
    console.log(data)

  }

  return (
    <GlobalContext.Provider
      value={{
        data,
        addUser,
        editUser,
        removeUser
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};